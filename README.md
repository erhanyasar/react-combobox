# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Notes About Project Details

- The project implemented with the details presented at [Coderbyte Coding Challenge](https://coderbyte.com/question/web-marketplace-g4sw1oiy76),
- Aim was to implement a combobox with a given spec,
- In a short period of time, custom combobox created from scratch using only React and Javascript,
- [Ionicons](https://ionic.io/ionicons/v4) used as CDN as an external library to display icons. (dangerouslySetInnerHTML is used to display HTML as well as an unicode characters)
- For additional/optional items part;
    - chevrons conditionally rendered to represent an **Animation** when combobox clicked,
    - `aria-label` attribute is used to alter **Accessibility**,
    - Lastly for **Testing** purposes, `combobox.test.js` created under `src/components/combobox` and initialized a single test case.
- There's more than a couple points can be improved hence they're not due to time constraints related to my sickness during provided time period.

# How to Setup This Project Up & Running?

- You can directly preview it via [Vercel](https://react-combobox.vercel.app/),

- First download `Node.js` from [NodeJS.org](https://nodejs.org/en) if you haven't,
- Then open terminal/shell of your choice and download this repository via GitLab with command `git clone https://gitlab.com/erhanyasar/react-combobox.git`,
- Then run `npm i`, `pnpm i`, or `yarn`
- Lastly run command `npm start`

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
