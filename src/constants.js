export const fruits = [
  { name: "Apple", icon: "&#127822;" },
  { name: "Banana", icon: "&#127820;" },
  { name: "Blueberry", icon: "&#129744;" },
  { name: "Mango", icon: "&#129389;" },
];
