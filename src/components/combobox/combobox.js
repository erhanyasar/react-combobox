import { useRef, useState } from "react";
import { fruits } from "../../constants";
import "./combobox.css";

function Combobox() {
  const selectItemListRef = useRef();
  const inputRef = useRef();
  const comboboxRef = useRef();
  const [filteredValues, setFilteredValues] = useState([...fruits]);
  const [selectedItem, setSelectedItem] = useState("");
  const [isComboboxOpen, setIsComboboxOpen] = useState(false);

  const handleClick = (e) => {
    setIsComboboxOpen(!isComboboxOpen);
    comboboxRef.current.focus();
    if (
      selectItemListRef.current.style.display === "none" ||
      selectItemListRef.current.style.display === ""
    ) {
      selectItemListRef.current.style.display = "flex";
      inputRef.current.style.backgroundColor = "white";
    } else {
      selectItemListRef.current.style.display = "none";
      inputRef.current.style.backgroundColor = "#E8E8E8";
    }
  };

  const handleFilterResults = (e) => {
    setFilteredValues(
      fruits.filter((fruit) => fruit.name.match(e.target.value))
    );
  };

  const handleItemSelect = (e) => {
    setSelectedItem(
      fruits.filter((fruit) => fruit.name.match(e.target.textContent.substr(2)))
    );
    selectItemListRef.current.style.display = "none";
    inputRef.current.style.backgroundColor = "#E8E8E8";
  };

  return (
    <>
      <div className="combobox" aria-label="custom combobox" ref={comboboxRef}>
        <div className="input-wrapper" onClick={handleClick}>
          <input
            type="text"
            ref={inputRef}
            className="input"
            aria-label="combobox autocomplete input"
            placeholder={
              selectedItem ? selectedItem[0].name : "Choose a Fruit:"
            }
            onChange={handleFilterResults}
          />
          <>
            {isComboboxOpen ? (
              <ion-icon
                name="chevron-up-outline"
                aria-label="icon chevron-up"
              ></ion-icon>
            ) : (
              <ion-icon
                name="chevron-down-outline"
                aria-label="icon chevron-down"
              ></ion-icon>
            )}
            &nbsp;
          </>
        </div>
        <ul
          className="select-items-list"
          aria-label="select list"
          ref={selectItemListRef}
        >
          {filteredValues?.map((fruit, index) => {
            return (
              <li
                key={index}
                className={selectedItem[0]?.name === fruit.name ? "active" : ""}
                aria-label="select list item"
                onClick={handleItemSelect}
              >
                <div dangerouslySetInnerHTML={{ __html: fruit.icon }}></div>
                <div className="select-item">{fruit.name}</div>
              </li>
            );
          })}
        </ul>
      </div>
    </>
  );
}

export default Combobox;
