import { render, screen } from "@testing-library/react";
import Combobox from "./combobox";
import { fruits } from "../../constants/fruits";

describe("Renders Combobox and Check for Component Elements", () => {
  describe("Renders Combobox", () => {
    it("Should render Combobox component successfully", () => {
      const { baseElement } = render(<Combobox />);
      expect(baseElement).toBeTruthy();
    });
  });
  describe("Check for Component Elements", () => {
    test("Should find combobox placeholder", () => {
      render(<Combobox />);
      const placeholderElement = screen.getByPlaceholderText("Choose a Fruit:");
      expect(placeholderElement).toBeInTheDocument();
    });
    it("Should find combobox items", () => {
      render(<Combobox />);
      const linkElement = screen.getByText(fruits[0].name);
      expect(linkElement).toBeInTheDocument();
    });
  });
});
