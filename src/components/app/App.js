import "./App.css";
import Combobox from "../combobox/combobox";

function App() {
  return (
    <div className="App">
      <Combobox />
    </div>
  );
}

export default App;
